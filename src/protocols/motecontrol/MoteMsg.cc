#include "protocols/motecontrol/MoteMsg.h"

namespace remote { namespace protocols { namespace motecontrol {

MoteMsg::MoteMsg(uint8_t*& buffer, uint32_t& buflen)
{
	buffer = this->read(buffer,buflen);
	deleteMsg = true;
}

MoteMsg::MoteMsg(MsgRequest& message)
        : type(MOTEMSGTYPE_REQUEST),
          message(&message),
          deleteMsg(false)
{
	setTimestamp();
}

MoteMsg::MoteMsg(MsgConfirm& message)
        : type(MOTEMSGTYPE_CONFIRM),
          message(&message),
          deleteMsg(false)
{
	setTimestamp();
}

MoteMsg::MoteMsg(MsgPayload& message)
        : type(MOTEMSGTYPE_DATA),
          message(&message),
          deleteMsg(false)
{
	setTimestamp();
}

MoteMsg::~MoteMsg()
{
	if (deleteMsg) {delete this->message;}
}


void MoteMsg::setTimestamp()
{
	struct timeval tv;

	if (gettimeofday(&tv, NULL) < 0)
		return;
	timestampSeconds = tv.tv_sec;
	timestampMillis = tv.tv_usec / 1000;
}

uint32_t MoteMsg::getLength()
{
	return sizeof(protocolVersion) + sizeof(timestampSeconds) +
	       sizeof(timestampMillis) + sizeof(type) + message->getLength();
}

uint8_t* MoteMsg::write(uint8_t* buffer, uint32_t& buflen)
{
	buffer = writevalue(protocolVersion,buffer,buflen);
	buffer = writevalue(timestampSeconds,buffer,buflen);
	buffer = writevalue(timestampMillis,buffer,buflen);
	buffer = writevalue(type,buffer,buflen);
	buffer = message->write(buffer,buflen);
	return buffer;
}

uint8_t* MoteMsg::read(uint8_t* buffer, uint32_t& buflen)
{
	buffer = readvalue(protocolVersion,buffer,buflen);
	// TODO: check protocolVersion
	buffer = readvalue(timestampSeconds,buffer,buflen);
	buffer = readvalue(timestampMillis,buffer,buflen);
	buffer = readvalue(type,buffer,buflen);
	switch (type)
	{
		case MOTEMSGTYPE_REQUEST:
			message = new MsgRequest(buffer,buflen);
			break;
		case MOTEMSGTYPE_CONFIRM:
			message = new MsgConfirm(buffer,buflen);
			break;
		case MOTEMSGTYPE_DATA:
			message = new MsgPayload(buffer,buflen);
			break;
	}
	return buffer;
}

void MoteMsg::print(FILE* s)
{
	fprintf(s,"MESSAGE MoteMsg\n");
	fprintf(s,"protocolVersion: %u\n",protocolVersion);
	fprintf(s,"type: ");
	switch (type)
	{
		case MOTEMSGTYPE_REQUEST:
			fprintf(s,"MOTEMSGTYPE_REQUEST\n");
			break;
		case MOTEMSGTYPE_CONFIRM:
			fprintf(s,"MOTEMSGTYPE_CONFIRM\n");
			break;
		case MOTEMSGTYPE_DATA:
			fprintf(s,"MOTEMSGTYPE_DATA\n");
			break;
		default:
			fprintf(s,"Invalid type!\n");
	}
	message->print(s);
}

uint32_t MoteMsg::getProtocolVersion()
{
	return protocolVersion;
}

uint8_t MoteMsg::getType()
{
	return type;
}

MsgRequest& MoteMsg::getRequest()
{
	if (type != MOTEMSGTYPE_REQUEST)
		__THROW__ ("Cannot getRequest when type is not MOTEMSGTYPE_REQUEST!");
	return *((MsgRequest*)message);
}

MsgConfirm& MoteMsg::getConfirm()
{
	if (type != MOTEMSGTYPE_CONFIRM)
		__THROW__ ("Cannot getConfirm when type is not MOTEMSGTYPE_CONFIRM!");
	return *((MsgConfirm*)message);
}

MsgPayload& MoteMsg::getData()
{
	if (type != MOTEMSGTYPE_DATA)
		__THROW__ ("Cannot getData when type is not MOTEMSGTYPE_DATA!");
	return *((MsgPayload*)message);
}
}}}
